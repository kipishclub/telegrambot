package bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class BingotiTelegramBot extends TelegramLongPollingBot {

    /** Логи */
    Logger logger = LoggerFactory.getLogger(this.getClass());

    // Пункты главного меню
    final private String menuQA = "Частые вопросы";
    final private String menuBuy = "Купить алмазы";
    final private String menuRng = "О генераторе случайных чисел";
    final private String menuHelp = "Решение проблем";

    final private String menuMain = "В начало";

    // Пункты меню Купить алмазы
    final private String menuBuy_RF = "Россия";
    final private String menuBuy_KZ = "Казахстан";

    // Пункты меню Купить алмазы в России
    final private String menuBuy_RF_Sberbank = "Сбербанк";
    final private String menuBuy_RF_Tinkoff = "Тинькофф";
    final private String menuBuy_RF_Alfabank = "Альфа-Банк";
    final private String menuBuy_RF_QIWI = "QIWI Кошелёк";

    // Пункты меню Купить алмазы в Казахстане
    final private String menuBuy_KZ_Kaspi = "Kaspi Bank";
    final private String menuBuy_KZ_Forte = "ForteBank";

    // Список частых вопросов и ответов
    private List<QuestionsAndAnswers> list_qa = new ArrayList<>();

    private String bank_info = "\nВ примечании к переводу *ОБЯЗАТЕЛЬНО УКАЖИТЕ ID ИГРОКА!*";
    public BingotiTelegramBot() {

        // Формируем Вопросы и Ответы
        list_qa.add(new QuestionsAndAnswers("1", "1. Как закончить игру?", "Возможно Вы пытались войти в приложение на одном устройстве, в то время когда на другом устройстве была запущена игра.\n" +
                                                                                                  "Не законченные игры удаляются через 1 час - 1 час 15 минут."));

        list_qa.add(new QuestionsAndAnswers("2", "2. Как считается процент комисии?", "Процент комиссии берётся с суммы ставок. В случае выигрыша с \"марсом\" сумма ставок - одна с победителя, две с проигравшего."));

        list_qa.add(new QuestionsAndAnswers("3", "3. Как вступить в клуб?", "Для того что-бы вступить в клуб необходимо знать ID клуба в который Вы хотите вступить и ID вашего агента. По умолчанию Вы уже состоите в клубе Bingoti, можно играть в нем."));

        list_qa.add(new QuestionsAndAnswers("4", "4. Как играть на алмазы?", "Для того, чтобы играть на алмазы, необходимо пройти полную регистрацию по номеру телефона."));

        list_qa.add(new QuestionsAndAnswers("5", "5. Как пройти полную регистрацию?", "В разделе Профиль нажмите надпись Полная регистрация, далее введите номер сотового телефона, нажмите кнопку Выслать СМС. Подождите несколько минут, когда придет сообщение с цифровым кодом. Код введите в соответствующее поле ввода, нажмите Войти."));

        list_qa.add(new QuestionsAndAnswers("6", "6. Что такое тайм банк?", "Тайм банк это возможность увеличения времени хода на 20 секунд. Во время хода тайм банком можно воспользоваться только один раз."));

        list_qa.add(new QuestionsAndAnswers("7", "7. Как посмотреть остаток тайм банков?", "Остаток тайм банков отображается в профиле - в левом верхнем углу возле изображения будильника."));

        list_qa.add(new QuestionsAndAnswers("8", "8. В чем отличие автоГСЧ от обычного ГСЧ?", "При включении опции АвтоГСЧ Вам не требуется передвигать ползунок для задания случайного числа для генератора значения зар, система сделает это автоматически."));

        list_qa.add(new QuestionsAndAnswers("9", "9. Что такое автоматический первый ход?", "Вариантов первого хода в партии не так много, данная опция позволяет системе делать первый ход за Вас."));

        list_qa.add(new QuestionsAndAnswers("10", "10. Что такое автовыброс фишек?", "Автовыброс фишек позволяет автоматически выбрасывать фишки во двор. Выброс фишек начинается с дальней лунки."));

        list_qa.add(new QuestionsAndAnswers("11", "11. Что такое ход одним нажатием?", "Данная опция позволяет программе делать ход фишкой одним нажатием на неё, в случае когда возможен только один вариант хода."));

        list_qa.add(new QuestionsAndAnswers("12", "12. Что такое показывать подсказки?", "Если активировать данную опцию, то на доске будут отмечаться возможные варианты ходов фишек."));

    }

    @Override
    public String getBotToken() {
        return "731214440:AAGXaMWq8-VXib7TxAV1iFR83oHHU4mt9s8";
    }

    @Override
    public String getBotUsername() {
        return "BingotiBot";
    }

    // Обработчик входящих сообщений
    @Override
    public void onUpdateReceived(Update update) {

        // Проверяем, есть ли сообщение
        if (update.hasMessage() && update.getMessage().hasText()) {

            // Создаём сообщение
            SendMessage message0 = new SendMessage();
            message0.setChatId(update.getMessage().getChatId());

            String msg_text1 = "";
            String msg_text2 = "";

            // Поиск ответа на частые вопросы по номеру
            boolean answer_found = false;
/*
            for (QuestionsAndAnswers qa : list_qa) {
                if (qa.getKey().equals(update.getMessage().getText())) {
                    message.setText("*" + qa.getQuestion() + "*\n" + qa.getAnswer());
                    answer_found = true;
                    break;
                }
            }
*/
            // Ответы на пункты главного меню
            if (!answer_found) {

                switch (update.getMessage().getText().trim()) {
                    /*
                    // Частые вопросы
                    case menuQA:
                        // Вывести список вопросов
                        String text_qa = "";
                        for (QuestionsAndAnswers qa : list_qa) {
                            text_qa += qa.getQuestion() + "\n";
                        }
                        text_qa += "*Отправьте номер вопроса ...*";
                        message.setText(text_qa);
                        answer_found = true;
                        break;
                    */
                    // Помощь
                    case menuHelp:
                        message0.setText("В случае возникновения проблем с зачислением на лицевой счёт алмазов - отправьте сообщение в Telegram на адрес @bingotiinfo");
                        addMenuHelp(message0);
                        answer_found = true;
                        break;
                    // Купить алмазы
                    case menuBuy:
                        message0.setText("Выберите");
                        addMenuBuy(message0);
                        answer_found = true;
                        break;
                    // В России
                    case menuBuy_RF:
                        message0.setText("Выберите");
                        addMenuBuyRF(message0);
                        answer_found = true;
                        break;
                    case menuBuy_RF_Sberbank:
                        message0.setText("Номер карты Сбербанка:");
                        msg_text1 = "5469 4400 3445 9900";
                        msg_text2 = bank_info;
                        answer_found = true;
                        break;
                    case menuBuy_RF_Tinkoff:
                        message0.setText("Номер карты банка Тинькофф:");
                        msg_text1 = "5536 9137 7824 4174";
                        msg_text2 = bank_info;
                        answer_found = true;
                        break;
                    case menuBuy_RF_Alfabank:
                        message0.setText("Номер карты банка Альфа-Банк:");
                        msg_text1 = "5559 4928 1364 3387";
                        msg_text2 = bank_info;
                        answer_found = true;
                        break;
                    case menuBuy_RF_QIWI:
                        message0.setText("Номер кошелька QIWI:");
                        msg_text1 = "+79331000888";
                        msg_text2 = bank_info;
                        answer_found = true;
                        break;
                    // В Казахстане
                    case menuBuy_KZ:
                        message0.setText("Выберите");
                        addMenuBuyKZ(message0);
                        answer_found = true;
                        break;
                    case menuBuy_KZ_Kaspi:
                        message0.setText("Номер карты Kaspi Bank:");
                        msg_text1 = "5169 4931 9118 7627";
                        msg_text2 = bank_info;
                        answer_found = true;
                        break;
                    case menuBuy_KZ_Forte:
                        message0.setText("Номер карты ForteBank:");
                        msg_text1 = "4042 4289 0755 5374";
                        msg_text2 = bank_info;
                        answer_found = true;
                        break;
                    // ГСЧ
                    case menuRng:
                        message0.setText("Генератор случайных чисел (ГСЧ)\n" +
                                "\n" +
                                "      В нашей платформе используется децентрализованный генератор случайных чисел, в дальнейшем ГСЧ.\n" +
                                "      Это означает, что никто кроме самих игроков не влияет на результат значений зар. Значения виртуальных зар при каждом броске зависят от выбора случайных чисел сделанных игроками.\n" +
                                "      Оба игрока одновременно выбирают по случайному числу от 1 до 999 путем перемещения слайдера на экране.\n" +
                                "      Затем, каждое число дополняется случайными символами из латинского алфавита так, чтобы длина строки стала 8 знаков, кроме того в строке используется один специальный символ. Для гаранта одновременности выбора случайных чисел и их неизменности впоследствии используется хеш функция SHA-1.\n" +
                                "      Для каждого исходного значения конечной строки, существует только одно значение хеш функции. Длина случайного слова выбрана равной 8 для того, чтобы для его вычисления из хеша путем перебора значений потребовалось несколько месяцев работы очень быстрого компьютера. В нашем приложении на всесь процесс генерации значений зар отводится 5 секунд.\n" +
                                "      После выбора случайных чисел и преобразования их в случайные слова, приложения игроков обмениваются значениями хеш функций этих слов. Затем приложения обмениваются самими словами. Далее, приложение вычисляет цифровую подпись (хеш функцию SHA-1) полученного от оппонента случайного слова и сравнивает со значением хеш функции полученным от него ранее. Таким образом гарантируется аутентичность переданной строки, т.е. когда игрок передал другому игроку строку - он этим действием подтвердил, что уже загадал определенное значение, и в последствии  не может изменить его. \n" +
                                "      Если, значения хеш функций соответствуют заявленным строкам, то это означает, что случайное слово полученное от другого игрока верное, и его можно использовать для дальнейшего вычисления значений зар, в противном случае игра останавливается. Таким образом происходит защита пользователей от хакерских атак, и гарантируется непредвзятость игровой платформы.\n" +
                                "      Далее оба случайных загаданных пользователями слова(строк) соединяются в одну строку, для которой вычисляется стандартная хеш функция SHA-1.\n" +
                                "      Результат хеш функции - число в шестнадцатеричной системе счисления, представляет из себя длинный набор букв и цифр.\n" +
                                "      В качестве значения первой зары из результата берется крайнее левое число попадающее в диапазон от 1 до 6.  В качестве значения второй зары - крайнее правое число.\n" +
                                "      Все загаданные пользователями строки в процессе игры сохраняются в истории пользователя, таким образом любой желающий может проверить достоверность результата. Для этого можно просто соединить загаданные пользователями строки для каждого конкретного хода и ввести значение полученной строки на любом онлайн сервисе по вычислению SHA-1, например можно использовать сайт http://www.sha1-online.com/ и по вычисленному значению увидеть конечный результат.");
                        answer_found = true;
                        break;
                }

            }

            if (!answer_found) {
                if (update.getMessage().getText().trim().toLowerCase().matches(".*вывести.*") || update.getMessage().getText().trim().toLowerCase().matches(".*вывод.*")) {
                    message0.setText("Никак, товарищь майор!");
                    answer_found = true;
                }
            }

            // Вывод главного меню, если ни один из ключей вопросов не найден
            if (!answer_found) {
                message0.setText("Нажмите на кнопку с вопросом ...");
                addMenuMain(message0);
            }

            // Отправка сообщения (Сообщение0 всегда отправляется, а Сообщение1, 2 если только есть текст для них)
            try {
                logger.info("> " + update.getMessage().getText());
                message0.setParseMode("Markdown"); // Включить разметку текста, чтобы звёздочками выделять жирный шрифт
                execute(message0);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            // Cообщение 1
            if (!msg_text1.isEmpty()) {
                SendMessage message1 = new SendMessage();
                message1.setChatId(update.getMessage().getChatId());
                message1.setText(msg_text1);
                try {
                    message1.setParseMode("Markdown");
                    execute(message1);
                } catch (TelegramApiException e) {
                }
            }

            // Cообщение 2
            if (!msg_text2.isEmpty()) {
                SendMessage message2 = new SendMessage();
                message2.setChatId(update.getMessage().getChatId());
                message2.setText(msg_text2);
                try {
                    message2.setParseMode("Markdown");
                    execute(message2);
                } catch (TelegramApiException e) {
                }
            }

        }

    }


    // Формирование кнопок главного меню
    public synchronized void addMenuMain(SendMessage sendMessage) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();

        // Первая строка клавиатуры
        KeyboardRow row1 = new KeyboardRow();
        // Добавляем кнопки в первую строчку клавиатуры
        row1.add(new KeyboardButton(menuBuy));

        // Вторая строка клавиатуры
        KeyboardRow row2 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row2.add(new KeyboardButton(menuRng));

//        // Третья строка клавиатуры
//        KeyboardRow row3 = new KeyboardRow();
//        // Добавляем кнопки во вторую строчку клавиатуры
//        row3.add(new KeyboardButton(menuQA));

        // Третья строка клавиатуры
        KeyboardRow row3 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row3.add(new KeyboardButton(menuHelp));

        // Добавляем все строчки клавиатуры в список
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        // и устанваливаем этот список нашей клавиатуре
        replyKeyboardMarkup.setKeyboard(keyboard);
    }


    // Формирование кнопок меню Купить алмазы
    public synchronized void addMenuBuy(SendMessage sendMessage) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();

        // Первая строка клавиатуры
        KeyboardRow row1 = new KeyboardRow();
        // Добавляем кнопки в первую строчку клавиатуры
        row1.add(new KeyboardButton(menuBuy_RF));

        // Вторая строка клавиатуры
        KeyboardRow row2 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row2.add(new KeyboardButton(menuBuy_KZ));

        // Третья строка клавиатуры
        KeyboardRow row3 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row3.add(new KeyboardButton(menuMain));

        // Добавляем все строчки клавиатуры в список
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        // и устанваливаем этот список нашей клавиатуре
        replyKeyboardMarkup.setKeyboard(keyboard);
    }

    // Формирование кнопок меню Купить в России
    public synchronized void addMenuBuyRF(SendMessage sendMessage) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();

        // Первая строка клавиатуры
        KeyboardRow row1 = new KeyboardRow();
        // Добавляем кнопки в первую строчку клавиатуры
        row1.add(new KeyboardButton(menuBuy_RF_Sberbank));

        // Вторая строка клавиатуры
        KeyboardRow row2 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row2.add(new KeyboardButton(menuBuy_RF_Tinkoff));

        // 3 строка клавиатуры
        KeyboardRow row3 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row2.add(new KeyboardButton(menuBuy_RF_Alfabank));

        // 4 строка клавиатуры
        KeyboardRow row4 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row2.add(new KeyboardButton(menuBuy_RF_QIWI));

        // Последняя строка клавиатуры
        KeyboardRow rowLast = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row3.add(new KeyboardButton(menuMain));

        // Добавляем все строчки клавиатуры в список
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        keyboard.add(row4);
        keyboard.add(rowLast);
        // и устанваливаем этот список нашей клавиатуре
        replyKeyboardMarkup.setKeyboard(keyboard);
    }


    // Формирование кнопок меню Купить в Казахстане
    public synchronized void addMenuBuyKZ(SendMessage sendMessage) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();

        // Первая строка клавиатуры
        KeyboardRow row1 = new KeyboardRow();
        // Добавляем кнопки в первую строчку клавиатуры
        row1.add(new KeyboardButton(menuBuy_KZ_Kaspi));

        // Вторая строка клавиатуры
        KeyboardRow row2 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row2.add(new KeyboardButton(menuBuy_KZ_Forte));

        // Третья строка клавиатуры
        KeyboardRow row3 = new KeyboardRow();
        // Добавляем кнопки во вторую строчку клавиатуры
        row3.add(new KeyboardButton(menuMain));

        // Добавляем все строчки клавиатуры в список
        keyboard.add(row1);
        keyboard.add(row2);
        keyboard.add(row3);
        // и устанваливаем этот список нашей клавиатуре
        replyKeyboardMarkup.setKeyboard(keyboard);
    }


    // Формирование кнопок меню Помощь
    public synchronized void addMenuHelp(SendMessage sendMessage) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();

        // Первая строка клавиатуры
        KeyboardRow row1 = new KeyboardRow();
        // Добавляем кнопки в первую строчку клавиатуры
        row1.add(new KeyboardButton(menuMain));

        // Добавляем все строчки клавиатуры в список
        keyboard.add(row1);
        // и устанваливаем этот список нашей клавиатуре
        replyKeyboardMarkup.setKeyboard(keyboard);
    }


}
