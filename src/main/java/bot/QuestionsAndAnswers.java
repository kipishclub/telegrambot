package bot;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionsAndAnswers {

    private String Key;
    private String Question;
    private String Answer;

    public QuestionsAndAnswers(String Key, String Question, String Answer) {
        this.Key = Key;
        this.Question = Question;
        this.Answer = Answer;
    }

}
